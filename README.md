# Proyecto SBD


### Tareas Pendientes.
1. Obtener los datos a partir del API y manejar el JSON. (Solo por dos horas)  - Juan Joseph
2. Generar el BoxPlot y ver los videos del Profe - Jose Javier
3. Subclasificacion de los datos de colonias a lo largo del tiempo - Jorge
4. Pocentaje por Temas. Grafica de Paster - Jose Javier y Juan Joseph
5. Mejorar las grafica de barras, demostrar años separado - Juan Joseph


#### Nota
Una vez que realices una de ellas, por favor eliminarlo. Infinitas Gracias
